
PDF = Nordpol-Richard_4.pdf

all: $(PDF)

clean:
	rm *.log *.aux *.fdb_latexmk *.fls

aspell: *.tex
	aspell -l de -t -c $<


%.pdf: *.tex
	echo $(PDF)
	latexmk $<

